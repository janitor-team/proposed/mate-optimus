mate-optimus (21.04.0-1) UNRELEASED; urgency=medium

  * New upstream release.

 -- Martin Wimpress <code@wimpress.io>  Tue, 31 Aug 2021 16:06:49 +0100

mate-optimus (20.10.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

  [ Mike Gabriel ]
  * debian/control: White-space fix.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 25 Aug 2020 09:50:05 +0200

mate-optimus (20.04.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches:
    + Drop 1001_ayatana-appindicator.patch. Applied upstream.
  * debian/control:
    + Bump DH compat level to version 13.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 23 May 2020 16:11:18 +0200

mate-optimus (19.10.4-2) unstable; urgency=medium

  * debian/control:
    + Add Rules-Requires-Root: field and set it to no.
    + Bump Standards-Version: to 4.5.0. No changes needed.
  * debian/upstream/metadata:
    + Drop obsolete fields Contact: and Name:.
    + Append .git suffix to URLs in Repository: field.
  * debian/control:
    + Add D (mate-optimus): gir1.2-ayatanaappindicator3-0.1. (LP: #1862166).
  * debian/patches:
    + Add 1001_ayatana-appindicator.patch. Prefer AyatanaAppIndicator3 over
      AppIndicator3.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 09 Feb 2020 21:01:34 +0100

mate-optimus (19.10.4-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release. (LP: #1843341)

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 28 Sep 2019 00:51:00 +0200

mate-optimus (19.10.3-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Add R: glew-utils (mate-optimus).
    + Add S: ubuntu-drivers-common (mate-optimus).
  * debian/copyright:
    + Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 14 Sep 2019 10:29:13 +0200

mate-optimus (19.10.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/install:
    + Add /etc.

  [ Mike Gabriel ]
  * debian/{compat,control}:
    + Use debhelper-compat notation. Bump to DH compat level version 12.
  * debian/control:
    + Bump Standards-Version: to 4.4.0. No changes needed.
  * debian/upstream/metadata:
    + Add file. Comply with DEP-12.
  * debian/changelog:
    + Post-upload fix of the actually used Standards-Version: with previous
      upload.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 02 Aug 2019 19:34:20 +0200

mate-optimus (18.04.0-2) unstable; urgency=medium

  [ Martin Wimpress ]
  * debian/control:
    + Add Suggests: nvidia-prime (mate-optimus).

  [ Mike Gabriel ]
  * debian/control:
    + Update Vcs-*: fields. Package has been migrated to salsa.debian.org.
    + Bump Standards-Version: to 4.1.5. No changes needed.
    + Drop pkg-mate-team Alioth mailing list from Uploaders: field.
  * debian/copyright:
    + Make Upstream-Name: field's value more human readable.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 12 Jul 2018 13:50:55 +0200

mate-optimus (18.04.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.

  * debian/control:
    + Temporarily have pkg-mate-team ML under Uploaders:.
    + Add myself to Uploaders: field.
    + Update Maintainer: field to debian-mate ML on lists.debian.org.
    + Rename pretty name of our team -> Debian+Ubuntu MATE Packaging Team.
    + Bump Standards-Version: to 4.1.3. No changes needed.

  [ Mike Gabriel ]
  * debian/copyright:
    + Use secure URI for copyright format.
  * debian/{control,compat}: Bump DH version level to 11.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 15 Feb 2018 12:47:23 +0100

mate-optimus (17.10.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Update Hompage URL.
    + Add D gir1.2-notify-0.7, python3-gi and python3-setproctitle.
  * debian/watch:
    + Update URL for GitHub.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 25 Sep 2017 11:06:39 +0200

mate-optimus (17.10.0-1) unstable; urgency=medium

  [ Aron Xu ]
  * New upstream release.

  [ Mike Gabriel ]
  * debian/control:
    + Bump Standards-Version: to 4.0.0. No changes needed.

 -- Mike Gabriel <sunweaver@debian.org>  Tue, 01 Aug 2017 14:35:13 -0400

mate-optimus (16.10.1-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New bug fix upstream release. (LP: #1632685)

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 13 Oct 2016 10:37:26 +0200

mate-optimus (16.10.0-2) unstable; urgency=medium

  * debian/watch:
    + Adapt to Bitbucket's new download URL scheme.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 07 Oct 2016 10:54:26 +0200

mate-optimus (16.10.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * New upstream release.
  * debian/control:
    + Add to D (mate-optimus): gir1.2-gtk-3.0
    + Bump Standards: to 3.9.8. No changes needed.
    + Use https URLs in Vcs-*: fields.

 -- Mike Gabriel <sunweaver@debian.org>  Mon, 13 Jun 2016 00:31:11 +0200

mate-optimus (1.0.0-1) unstable; urgency=medium

  [ Martin Wimpress ]
  * Initial release. (Closes: #793379).

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 24 Jul 2015 22:36:21 +0200
